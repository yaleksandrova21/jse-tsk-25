package ru.yaleksandrova.tm.command.system;

import org.jetbrains.annotations.NotNull;
import ru.yaleksandrova.tm.command.AbstractCommand;

public final class AboutDisplayCommand extends AbstractCommand {

    @NotNull
    @Override
    public String name() {
        return "about";
    }

    @NotNull
    @Override
    public String arg() {
        return "-a";
    }

    @NotNull
    @Override
    public String description() {
        return "Display developer information";
    }

    @Override
    public void execute() {
        System.out.println("[ABOUT]");
        @NotNull final String developer = serviceLocator.getPropertyService().getDeveloperName();
        System.out.println("Developer: " + developer);
        @NotNull final String email = serviceLocator.getPropertyService().getDeveloperEmail();
        System.out.println("E-mail: " + email);
    }

}
