package ru.yaleksandrova.tm.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import ru.yaleksandrova.tm.repository.AbstractRepository;

import java.util.UUID;

@Getter
@Setter
@NoArgsConstructor
public class AbstractEntity {

    @NotNull
    protected String id = UUID.randomUUID().toString();

}
